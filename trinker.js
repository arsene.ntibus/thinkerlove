const {forEach} = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },
  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },
  allMale: function (p) {
    /* return p.filter(people) => (p.gender = "Male"); */

    return p.filter(function (homme) {
      let result;
      result = homme.gender === "Male";
      return result;
    });
  },
  allFemale: function (p) {
    return p.filter(function (femme) {
      return femme.gender === "Female";
    });
  },
  nbOfMale: function (p) {
    return this.allMale(p).length;
  },
  nbOfFemale: function (p) {
    return this.allFemale(p).length;
  },
  nbOfMaleInterest: function (p) {
    return p.filter((person) => person.looking_for === "M").length;
  },
  nbOfFemaleInterest: function (p) {
    return p.filter((person) => person.looking_for === "F").length;
  },
  salaryOver2k: function (p) {
    return p.filter((person) => person.income.substring(1) > 2000).length;
  },
  dramaLovers: function (p) {
    return p.filter((person) => person.pref_movie.includes("Drama")).length;
  },
  femaleScifiLovers: function (p) {
    return p.filter((person) => person.pref_movie.includes("Sci-Fi") && person.gender === "Female")
      .length;
  },
  documentariesOver1482: function (p) {
    return p.filter(
      (person) =>
        person.pref_movie.includes("Documentary") && parseInt(person.income.substring(1)) > 1482,
    ).length;
  },

  peopleOver4k: function (p) {
    let over4k = [];
    console.log(over4k);

    for (let i = 0; i < p.length; i++) {
      if (p[i].income.substring(1) > 4000) {
        over4k.push(`${p[i].last_name} ${p[i].first_name} ${p[i].id} ${p[i].income}`);
      }
    }
    /*  let max =p.filter(over4k=>p.income.substring(1)) */

    return over4k;
  },

  match: function (p) {
    return "not implemented".red;
  },
};
